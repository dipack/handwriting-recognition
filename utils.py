import numpy as np


# Removing features that have a variance of 0, as they will not aid the
# prediction process
def clean_data(X):
    variance = np.var(X, axis=0)
    columns_to_delete = []
    for idx, column in enumerate(variance):
        if column == float(0.00):
            columns_to_delete.append(idx)
    return np.delete(X, columns_to_delete, axis=1)

def print_submitter_info():
    print("=================================")
    print("UBITName      : dipackpr")
    print("Person number : 50291077")
    print("=================================")
    return
