from linear_regression import main as lr_main
from logistic_regression import main as logr_main
from neural_network import main as nn_main

def main():
    print("WARNING: You will need to change the `pick_fileset` functions input")
    print("         in each file individually to change which dataset it uses!")
    lr_main()
    logr_main()
    nn_main()
    return

if __name__ == "__main__":
    main()
