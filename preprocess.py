#!/usr/bin/env python3
import os
import sys
import pandas as pd
import numpy as np
from utils import clean_data

def read_features_csv(filename="../data/HumanObserved-Dataset/HumanObserved-Features-Data/HumanObserved-Features-Data.csv"):
    """
    Three return values: The features dataframe as-is,
                        just the 9 features for each person,
                        and the identifiers for the people
    The sort order for the features, and the labels is to be kept the same
    """
    # Hard coding this value, as we know that the GSC dataset does not have an index
    if "GSC" in filename:
        features_df = pd.read_csv(filename, sep=",", index_col=None, skiprows=[0], header=None)
    else:
        features_df = pd.read_csv(filename, sep=",", index_col=0, skiprows=[0], header=None)
    features_only = features_df.iloc[:, 1:].values
    labels = features_df.iloc[:, 0].values
    return features_df, features_only, labels

def read_target_csv(diff_file="../data/HumanObserved-Dataset/HumanObserved-Features-Data/diffn_pairs.csv",
        same_file="../data/HumanObserved-Dataset/HumanObserved-Features-Data/same_pairs.csv",
        min_value=10000):
    same_df = pd.read_csv(same_file, sep=",", index_col=None, skiprows=[0], header=None)
    # Reading a maximum of 10,000 rows of both datasets, as we do not want to spend forever computing LR
    rows = min(same_df.shape[0], min_value)
    # Reading equal number of rows from the diff file, as from the same file
    same_df = pd.read_csv(same_file, sep=",", index_col=None, skiprows=[0], header=None, nrows=rows)
    diff_df = pd.read_csv(diff_file, sep=",", index_col=None, skiprows=[0], header=None, nrows=rows)
    target_df = diff_df.append(same_df)
    return target_df, diff_df, same_df

def combine_features(features, labels, relation_df, concat_mode=True):
    combined_features = []
    target_values = []
    for row in relation_df.itertuples():
        user_1     = str(row._1)
        user_2     = str(row._2)
        target     = int(row._3)
        user_1_idx = labels.tolist().index(user_1)
        user_2_idx = labels.tolist().index(user_2)
        user_1_f   = features[user_1_idx]
        user_2_f   = features[user_2_idx]
        if concat_mode:
            val = np.append(user_1_f, user_2_f)
        else:
            val = np.abs(np.subtract(user_1_f, user_2_f))
        combined_features.append(val)
        target_values.append(target)
    return combined_features, target_values

def save_to_csv(features, target):
    features_filename = "features.csv"
    target_filename = "target.csv"
    np.savetxt(features_filename, features, delimiter=",")
    np.savetxt(target_filename, target, delimiter=",")
    return True

def read_from_csv(features_filename="features.csv", target_filename="target.csv"):
    if not os.path.isfile(features_filename):
        print("{} does not exist!".format(features_filename))
        return None, None
    if not os.path.isfile(target_filename):
        print("{} does not exist!".format(target_filename))
        return None, None
    features = np.genfromtxt(features_filename, delimiter=",")
    target = np.genfromtxt(target_filename, delimiter=",")
    return features, target

def preprocess(features_filename, diff_file, same_file, concat_mode=True, min_value=10000, ignore_local=False):
    print("Reading data from filesets; Please be patient...")
    X, y = None, None
    if not ignore_local:
        res0, res1 = read_from_csv()
        if isinstance(res0, (list, np.ndarray)):
            print("Reading data from local files")
            X, y = res0, res1
    else:
        f, features_only, labels         = read_features_csv(features_filename)
        target_df, diff_df, same_df      = read_target_csv(diff_file, same_file, min_value)
        shuffled_target_df               = target_df.sample(frac=1).reset_index(drop=True)
        combined_features, target_values = combine_features(features_only, labels, shuffled_target_df, concat_mode)
        X, y                             = combined_features, target_values
        if not ignore_local and save_to_csv(combined_features, target_values):
            print("Successfully saved data to CSV files in local directory!")
    print("Done processing data!")
    return X, y

def pick_fileset(choice=1):
    if choice == 1:
        filename  = "../data/HumanObserved-Dataset/HumanObserved-Features-Data/HumanObserved-Features-Data.csv"
        diff_file = "../data/HumanObserved-Dataset/HumanObserved-Features-Data/diffn_pairs.csv"
        same_file = "../data/HumanObserved-Dataset/HumanObserved-Features-Data/same_pairs.csv"
    else:
        filename  = "../data/GSC-Dataset/GSC-Features-Data/GSC-Features.csv"
        diff_file = "../data/GSC-Dataset/GSC-Features-Data/diffn_pairs.csv"
        same_file = "../data/GSC-Dataset/GSC-Features-Data/same_pairs.csv"
    return filename, diff_file, same_file

def del_bad_cols(X, y):
    print(len(X), len(X[0]))
    return

def main():
    min_value = 3000
    concat_mode = True
    filename, diff_file, same_file = pick_fileset(2)
    X, y = preprocess(filename, diff_file, same_file, concat_mode, min_value, True)
    del_bad_cols(X, y)
    return

if __name__ == "__main__":
    main()
