#!/usr/bin/env python3
import os
import sys
import math
import numpy as np
from sklearn.cluster import KMeans
from utils import print_submitter_info, clean_data
from preprocess import preprocess, pick_fileset

# Partitioning the training and target datasets, into actual training,
# testing. and validation datasets
def partition(X, y):
    X = clean_data(X)

    rows = X.shape[0]
    num_training = int(0.7 * rows)

    X_train = X[:num_training, :]
    X_test = X[num_training:, :]

    y_train = y[:num_training]
    y_test = y[num_training:]
    return X, X_train, X_test, y, y_train, y_test

# Fitting the dataset to cluster around an arbitrary number of centers,
# and help us find the means of each feature, in each cluster
def kmeans_cluster(training_data, num_of_clusters=10):
    return KMeans(num_of_clusters).fit(training_data)

# Applies the Gaussian Basis Function to each row of data,
# using the means for each row received from the KMeans clusters,
# and the calculated variance for that feature
def calculate_gaussian_basis_out(data_row, mean_row, variance_inv):
    sub = np.subtract(data_row, mean_row)
    mult = np.dot(variance_inv, np.transpose(sub))
    final = np.dot(sub, mult)
    gbf = math.exp(-0.5 * final)
    return gbf

# Generating the covariance matrix for each feature in the dataset,
# w.r.t to itself only, and setting all the other values to zero
# CoVar_Matrix = [[ v1 0 0 .... 0 ]
#                 [ 0 v2 0 .... 0 ]
#                 .
#                 .
#                 .
#                 [ 0 0 0 .... vN ]]
#
def generate_covariance_matrix(data):
    variance = np.var(data, axis=0)
    rows, columns = data.shape
    covar = np.zeros((columns, columns))
    # We do not care about covar with other columns, simply the variance in each column exclusively
    for idx in range(columns):
        covar[idx][idx] = variance[idx]
    covar = np.dot(200, covar)
    return covar

# Applying the basis functions to each row in the dataset
def generate_design_matrix(data, mean_matrix, covar_matrix):
    rows = data.shape[0]
    columns = mean_matrix.shape[0]
    basis_fn_matrix = np.zeros((rows, columns))
    covar_inverse = np.linalg.inv(covar_matrix)
    for col in range(columns):
        for row in range(rows):
            basis_fn_matrix[row][col] = calculate_gaussian_basis_out(data[row], mean_matrix[col], covar_inverse)
    return basis_fn_matrix

# Generating weights by using Moore-Penrose Pseudo Inverse technique
def generate_weights(design_matrix, training_target_values, reg_coeff):
    num_of_bfns = design_matrix.shape[1]
    ident_matrix = np.identity(num_of_bfns)

    for idx in range(num_of_bfns):
        ident_matrix[idx][idx] = reg_coeff

    design_matrix_t = np.transpose(design_matrix)
    design_matrix_sq = np.dot(design_matrix_t, design_matrix)
    regularised_design_matrix_sq = np.add(ident_matrix, design_matrix_sq)
    inverted_reg_design_m = np.linalg.inv(regularised_design_matrix_sq)

    moore_penrose_pseudo_inverse = np.dot(inverted_reg_design_m, design_matrix_t)

    weights = np.dot(moore_penrose_pseudo_inverse, training_target_values)

    return weights

# Using the calculated weights, and the dataset, to calculate target values
def calculate_target(data, weights):
    return np.dot(np.transpose(weights), np.transpose(data))

# Compares the accuracy of the calculated target values,
# versus the actual target values,
# using the Root Mean Squared error function
def calculate_accuracy(calculated_data, actual_probabilities):
    if len(calculated_data) != len(actual_probabilities):
        print("The length of the calculated values and actual probabilities do not match!")
        print("Length of calculated values:", len(calculated_data))
        print("Length of actual probabilities:", len(actual_probabilities))
        sys.exit(127)
    add_result = 0.0
    counter = 0
    for idx in range(len(calculated_data)):
        add_result = add_result + pow(actual_probabilities[idx] - calculated_data[idx], 2)
        if int(np.around(calculated_data[idx])) == int(actual_probabilities[idx]):
            counter = counter + 1
    accuracy = (counter / len(calculated_data)) * 100
    error = math.sqrt(add_result / len(calculated_data))
    return accuracy, error

def gradient_descent_model(learning_rate, regularisation_coeff, weights_m, design_m, target_values):
    weights_multiplier = 300
    num_datapoints = 100
    accuracy = 0.00
    error = 0.00
    errors = []
    weights_mod = np.dot(weights_multiplier, weights_m)
    print("Running Gradient Descent based optimiser; DO NOT QUIT!")
    for idx in range(num_datapoints):
        current_target = np.dot(np.transpose(weights_mod), design_m[idx])
        diff = target_values[idx] - current_target
        delta = -np.dot(diff, design_m[idx])
        regularised_delta = np.dot(regularisation_coeff, weights_mod)

        error_delta = np.add(regularised_delta, delta)
        weights_delta = -np.dot(learning_rate, error_delta)

        weights_next = weights_mod + weights_delta
        weights_mod = weights_next

        target = calculate_target(design_m, weights_next)
        accuracy, error = calculate_accuracy(target, target_values)
        errors.append(error)
    # import matplotlib.pyplot as plt
    # plot_1 = plt.figure()
    # lr_plot = plot_1.add_subplot(111)
    # lr_plot.plot(range(num_datapoints), errors)
    # plt.xlabel("Epoch")
    # plt.ylabel("E(RMS)")
    # plt.title("E(RMS) - Linear Regression")
    # plt.show()
    return accuracy, error


def build_components(X, y, X_train, y_train, num_clusters, regularisation_coeff):
    kmeans = kmeans_cluster(X_train, num_clusters)
    # 10 clusters, each with 41 columns, and each column has a single mean, repped by the below
    # cluster centes
    mean_matrix = kmeans.cluster_centers_
    # Covariance of each column w.r.t. to itself; we do not care about inter-column covariance
    covar = generate_covariance_matrix(X)
    # Design matrix
    design_m = generate_design_matrix(X_train, mean_matrix, covar)
    # Generating weights by using Moore-Penrose Pseudo Inverser technique
    weights = generate_weights(design_m, y_train, regularisation_coeff)
    return kmeans, mean_matrix, covar, design_m, weights

def run_linear_regression(features_filename,
                            diff_file,
                            same_file,
                            num_clusters=10,
                            regularisation_coeff=10,
                            learning_rate=0.01,
                            concat_mode=True):
    X, y = preprocess(features_filename, diff_file, same_file, concat_mode, min_value=10000, ignore_local=True)

    X, X_train, X_test, y, y_train, y_test = partition(X, y)

    kmeans, mean_matrix, covar, training_design_m, weights = build_components(X, y, X_train, y_train, num_clusters, regularisation_coeff)
    # Generating the testing and validation basis function matrices as well
    testing_design_m = generate_design_matrix(X_test, mean_matrix, covar)

    # Gradient Descent Mode
    print("=================================")
    print("GRADIENT DESCENT MODE | Concat Mode?", concat_mode)
    print("=================================")
    print("Regularisation coefficient : ", regularisation_coeff)
    print("Learning rate              : ", learning_rate)
    weights_mod = np.reshape(weights, (-1))
    y_mod = np.reshape(y_train, (-1))
    accuracy, error = gradient_descent_model(learning_rate, regularisation_coeff, weights_mod, training_design_m, y_mod)
    print("Training => Accuracy: {0}, E(RMS): {1}".format(accuracy, error))
    weights_mod = np.reshape(weights, (-1))
    y_mod = np.reshape(y_test, (-1))
    accuracy, error = gradient_descent_model(learning_rate, regularisation_coeff, weights_mod, testing_design_m, y_mod)
    print("Testing => Accuracy: {0}, E(RMS): {1}".format(accuracy, error))
    return


def main():
    print_submitter_info()
    print("=================================")
    print("LINEAR REGRESSION")
    print("=================================")
    filename, diff_file, same_file = pick_fileset(2)
    num_clusters = 5
    regularisation_coeff = 2
    learning_rate = 0.09
    run_linear_regression(filename, diff_file, same_file, num_clusters, regularisation_coeff, learning_rate, True)
    run_linear_regression(filename, diff_file, same_file, num_clusters, regularisation_coeff, learning_rate, False)
    return

if __name__=='__main__':
    main()
