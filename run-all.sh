#!/usr/bin/env bash

python linear_regression.py   |& tee linr.all.human.out
python logistic_regression.py |& tee logr.all.human.out
python neural_network.py      |& tee nn.all.human.out
