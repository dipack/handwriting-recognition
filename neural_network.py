#!/usr/bin/env python3
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
import pandas as pd
from utils import print_submitter_info
from preprocess import preprocess, pick_fileset
from keras import losses
from keras.callbacks import EarlyStopping, TensorBoard
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.utils import to_categorical

# Partitioning the training and target datasets, into actual training,
# testing. and validation datasets
def partition(X, y):
    # Not cleaning the data
    # X = clean_data(X)
    X = np.array(X)

    rows = len(X)
    num_training = int(0.6 * rows)

    X_train = X[:num_training, :]
    X_test = X[num_training:, :]

    y = to_categorical(y, 2)
    y_train = y[:num_training]
    y_test = y[num_training:]
    return X, X_train, X_test, y, y_train, y_test


def build_model(X):
    first_layer_nodes = 256
    second_layer_nodes = 2
    drop_out_probability = 0.1
    num_features = len(X[0])

    model = Sequential()
    model.add(Dense(first_layer_nodes, input_shape=(num_features, )))
    model.add(Activation('relu'))
    model.add(Dropout(drop_out_probability))
    model.add(Dense(second_layer_nodes))
    model.add(Activation('sigmoid'))

    model.summary()

    model.compile(optimizer='rmsprop', loss=losses.categorical_crossentropy, metrics=['accuracy'])
    return model


def train_model(X_train, y_train):
    model = build_model(X_train)

    validation_data_split = 0.2
    num_epochs = 1000
    model_batch_size = 128
    tb_batch_size = 32
    early_patience = 75

    callback_tensorboard = TensorBoard(log_dir='logs', batch_size=tb_batch_size, write_graph=True)
    callback_earlystopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=early_patience)

    history = model.fit(X_train
                        , y_train
                        , validation_split=validation_data_split
                        , epochs=num_epochs
                        , batch_size=model_batch_size
                        , callbacks=[callback_earlystopping, callback_tensorboard]
                        )

    # import matplotlib.pyplot as plt
    # history_df = pd.DataFrame(history.history)
    # history_df['val_loss'].plot(x='Epoch', y='Loss', subplots=True, grid=True, figsize=(8, 14))
    # plt.show()
    return model

def test_model(model, X_test, y_test):
    correct_ctr = 0
    predicted_values = []
    for test_input, actual_target in zip(X_test, y_test):
        reshaped_test_input = np.reshape(test_input, (1, -1))
        predicted_value = model.predict(reshaped_test_input)
        predicted_values.append(predicted_value)
        if predicted_value.argmax() == actual_target.argmax():
            correct_ctr += 1
    accuracy = correct_ctr / len(y_test)
    return predicted_values, accuracy

def run_neural_network(filename, diff_file, same_file, concat_mode, min_value):
    print("=================================")
    print("FULLY CONNECTED NEURAL NET | Concat mode?", concat_mode)
    print("=================================")
    ignore_local_csv = True
    X, y = preprocess(filename, diff_file, same_file, concat_mode, min_value, ignore_local_csv)
    X, X_train, X_test, y, y_train, y_test = partition(X, y)

    model = train_model(X_train, y_train)
    loss, accuracy = model.evaluate(X_test, y_test, batch_size=1)
    predicted_values, manual_accuracy = test_model(model, X_test, y_test)
    print("Evaluated accuracy (from Keras on testing data): {0}, Manually calculated accuracy (on same testing data): {1}".format(accuracy, manual_accuracy))
    return

def main():
    print_submitter_info()
    min_value = 20000
    features_filename, diff_file, same_file = pick_fileset(2)
    run_neural_network(features_filename, diff_file, same_file, True, min_value)
    run_neural_network(features_filename, diff_file, same_file, False, min_value)
    return

if __name__ == "__main__":
    main()
