import numpy as np
from preprocess import pick_fileset, preprocess
from utils import clean_data, print_submitter_info

# Partitioning the training and target datasets, into actual training,
# testing. and validation datasets
def partition(X, y):
    X = clean_data(X)

    rows = X.shape[0]
    num_training = int(0.7 * rows)

    X_train = X[:num_training, :]
    X_test = X[num_training:, :]

    y_train = y[:num_training]
    y_test = y[num_training:]
    return X, X_train, X_test, y, y_train, y_test

def sigmoid(input_m):
    numerator = 1
    denominator = np.add(1, np.exp(np.dot(-1, input_m)))
    return np.divide(numerator, denominator)

def log_likelihood(features, target, weights):
    scores = np.dot(features, weights)
    log_like = np.sum(target * scores - np.log(1 + np.exp(scores)) )
    return log_like

def logistic_regression_sgd(features, target, num_steps, learning_rate, add_bias=False):
    if add_bias:
        intercept = np.ones((features.shape[0], 1))
        features = np.hstack((intercept, features))
    weights = np.zeros(features.shape[1])
    for step in range(num_steps):
        scores = np.dot(features, weights)
        predictions = sigmoid(scores)

        # Update weights with gradient
        output_error_signal = target - predictions
        gradient = np.dot(features.T, output_error_signal)
        weights += learning_rate * gradient
    return weights

def calculate_accuracy(calculated_data, actual_probabilities):
    if len(calculated_data) != len(actual_probabilities):
        print("The length of the calculated values and actual probabilities do not match!")
        print("Length of calculated values:", len(calculated_data))
        print("Length of actual probabilities:", len(actual_probabilities))
        sys.exit(127)
    add_result = 0.0
    counter = 0
    for idx in range(len(calculated_data)):
        add_result = add_result + pow(actual_probabilities[idx] - calculated_data[idx], 2)
        if int(np.around(calculated_data[idx])) == int(actual_probabilities[idx]):
            counter = counter + 1
    accuracy = (counter / len(calculated_data)) * 100
    error = np.sqrt(add_result / len(calculated_data))
    return accuracy, error

def calculate_targets(X, y, weights, with_bias=False):
    if with_bias:
        ones_column = np.ones((len(X), 1))
        data = np.hstack((ones_column, X))
    else:
        data = X
    unactivated_predictions = np.dot(data, weights)
    predictions = np.round(sigmoid(unactivated_predictions))
    accuracy, error = calculate_accuracy(predictions, y)
    return accuracy, error

def run_logistic_regression(features_filename,
        diff_file,
        same_file,
        learning_rate=0.01,
        concat_mode=True):

    with_bias = True
    num_steps = 20000

    X, y = preprocess(features_filename, diff_file, same_file, concat_mode, min_value=5000, ignore_local=True)
    X, X_train, X_test, y, y_train, y_test = partition(X, y)
    weights = logistic_regression_sgd(X_train, y_train, num_steps, learning_rate, with_bias)

    print("=================================")
    print("GRADIENT DESCENT MODE | Concat Mode?", concat_mode)
    print("=================================")
    print("Learning rate              : ", learning_rate)
    accuracy, error = calculate_targets(X_train, y_train, weights, with_bias)
    print("Training: Accuracy: {0}, E(RMS): {1}".format(accuracy, error))
    accuracy, error = calculate_targets(X_test, y_test, weights, with_bias)
    print("Testing: Accuracy: {0}, E(RMS): {1}".format(accuracy, error))

    return

def main():
    print_submitter_info()
    print("=================================")
    print("LOGISTIC REGRESSION")
    print("=================================")
    filename, diff_file, same_file = pick_fileset(1)
    learning_rate = 0.03
    run_logistic_regression(filename, diff_file, same_file, learning_rate, True)
    run_logistic_regression(filename, diff_file, same_file, learning_rate, False)
    return

if __name__ == "__main__":
    main()
